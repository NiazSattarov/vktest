package niaz.vktest.data;

/**
 * The class for each user message
 */
public class MyMessage {
    int         id          = 0;        // user id
    String      name        = null;     // user name
    String      message     = null;     // message text
    String      img         = null;     // image URL
    String      date        = null;     // date of message

    /**
     * Constructor
     * @param id
     * @param message
     */
    public MyMessage(int id, String message)
    {
        this.id = id;
        this.message = message;
    }

    // Setters, getters

    public int getId()
    {
        return id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public String getMessage()
    {
        return message;
    }

    public void setImg(String img)
    {
        this.img = img;
    }

    public String getImg()
    {
        return img;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getDate()
    {
        return date;
    }
}