package niaz.vktest.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import niaz.vktest.R;

/**
 * ListView adapter. It was possible to use some new view such as RecycleView
 * instead of ListView though
 */
public class MyAdapter extends ArrayAdapter<MyMessage> {

    private ArrayList<MyMessage>    messagesList    = new ArrayList<>();
    private ImageView               imageView       = null;

    /**
     * Constructor
     * @param context
     * @param textViewResourceId
     * @param objects
     */
    public MyAdapter(Context context, int textViewResourceId, ArrayList<MyMessage> objects) {
        super(context, textViewResourceId, objects);
        messagesList = objects;
    }

    // Setters, getters
    @Override
    public int getCount() {
        return super.getCount();
    }

    /**
     * Get view. Set values for each row.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.messages, null);

        TextView textViewName = (TextView) v.findViewById(R.id.name);
        textViewName.setText(messagesList.get(position).getName());

        TextView textViewMessage = (TextView) v.findViewById(R.id.message);
        textViewMessage.setText(messagesList.get(position).getMessage());

        TextView textViewDate = (TextView) v.findViewById(R.id.textViewDate);
        textViewDate.setText(messagesList.get(position).getDate());

        imageView = (ImageView) v.findViewById(R.id.imageViewPhoto);
        String url = messagesList.get(position).getImg();
        Bitmap bitmap = MyCache.getFromCache(url);

        // Image is not downloaded
        if (bitmap == null) {
            new DownloadImage(imageView).execute(url);
        }
        // Set image from the cache
        else {
            imageView.setImageBitmap(bitmap);
        }
        return v;
    }
}