package niaz.vktest.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * Async task to download image
 */
public class DownloadImage extends AsyncTask<String, Void, Bitmap> {

    private ImageView   imageView   = null; // ImageView for user's photo
    private String      url         = null; // URL of user's photo

    /**
     * Constructor
     * @param imageView
     */
    public DownloadImage(ImageView imageView) {
        this.imageView = imageView;
    }

    /**
     * Async task to read bitmap of user's photo
     * @param urls
     * @return
     */
    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        this.url = url;
        Bitmap bitmap = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * Async task is finishing
     * @param result
     */
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            imageView.setImageBitmap(result);
            MyCache.addToCache(url, result);
        }
    }
}