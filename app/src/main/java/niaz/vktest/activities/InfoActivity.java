package niaz.vktest.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiGetMessagesResponse;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.util.VKUtil;
import org.json.JSONArray;
import org.json.JSONException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import niaz.vktest.data.MyAdapter;
import niaz.vktest.data.MyMessage;
import niaz.vktest.R;

/**
 * Created by Niaz Sattarov on 29.11.2016.
 * Login to Vkontakte and  getting user's messages
 */
public class InfoActivity extends AppCompatActivity {
    private String[]                scope = {VKScope.MESSAGES};         // scope for VK
    private ListView                listView = null;                    // ListView
    private ArrayList<MyMessage>    messagesList = new ArrayList<>();   // List of messages

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        String[] fp = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        System.out.println("App fingerprint=" + Arrays.asList(fp));

        VKSdk.login(this, scope);
    }

    /**
     * On Activity result
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {

                listView = (ListView) findViewById(R.id.listView);
                VKRequest vkRequest = VKApi.messages().get(VKParameters.from(VKApiConst.COUNT, 15));

                // Successful authorization
                vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);
                        getMessages(response);
                    }
                });
            }

            // Authorization error
            @Override
            public void onError(VKError error) {
                Toast.makeText(getApplicationContext(), "Authorization error", Toast.LENGTH_LONG).show();
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Get messages
     * @param response
     */
    private void getMessages(VKResponse response) {
        // Set my adapter
        final MyAdapter myAdapter = new MyAdapter(getApplicationContext(), R.layout.messages, messagesList);
        listView.setAdapter(myAdapter);

        // Get list of messages
        VKApiGetMessagesResponse vkApiGetMessagesResponse = (VKApiGetMessagesResponse) response.parsedModel;
        VKList<VKApiMessage> vkList = vkApiGetMessagesResponse.items;

        // Iterate through messages
        for (VKApiMessage vkApiMessage : vkList) {
            final MyMessage myMessage = new MyMessage(vkApiMessage.user_id, vkApiMessage.body);

            String dateString = DateFormat.format("dd-MM-yyyy", new Date(vkApiMessage.date * 1000)).toString();
            myMessage.setDate(dateString);

            // Get user info
            int id = vkApiMessage.user_id;
            VKRequest req = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "photo_200", VKApiConst.USER_ID, id));
            req.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    super.onComplete(response);
                    VKApiUser user;
                    try {
                        JSONArray s = response.json.getJSONArray("response");
                        user = new VKApiUser(s.getJSONObject(0));
                        myMessage.setName(user.first_name + " " + user.last_name);
                        myMessage.setImg(user.photo_200);
                        myAdapter.add(myMessage);
                    } catch (JSONException e) {

                    }
                }
            });
        }
    }

    /**
     * Create top menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Process item selection in menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            VKSdk.logout();
            Intent intent = new Intent(InfoActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
