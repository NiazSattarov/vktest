package niaz.vktest.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import niaz.vktest.R;

/**
 * Created by Niaz Sattarov on 29.10.2016. My main activity.
 * This app provides login to Vkontakte and getting user's messages.
 * I use simple cache for images. It allows to show images without delay
 * after changing screen orientation.
 * It was possible to use a library such as Picasso instead.
 */
public class MainActivity extends Activity{

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.buttonStart);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}